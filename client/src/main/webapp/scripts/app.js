'use strict';

angular.module('sightApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate',
  'leaflet-directive',
  'ui.bootstrap',
  'angular-growl'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', { templateUrl: 'views/main.html', controller: 'MainCtrl' })
      .when('/control', { templateUrl: 'views/client.html', controller: 'ClientController' })
      .when('/sights/:id', { templateUrl: 'views/sight/show.html', controller: 'SightController' })
      .otherwise({ redirectTo: '/' });
  })
  .filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  }).config(['growlProvider', function(growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.onlyUniqueMessages(false);
  }]);