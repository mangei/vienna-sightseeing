package io.geier.viennasightseeing.client.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.geier.viennasightseeing.client.command.CurrentDataVolumeCommand;
import io.geier.viennasightseeing.client.config.ConnectionInfo;
import io.geier.vbs.VotingResult;
import io.geier.vbs.VotingResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ClientRepository {

    private RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    public ClientRepository() {
        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
        converters.add(new MappingJackson2HttpMessageConverter());
        restTemplate = new RestTemplate(converters);
    }

    public void push(VotingResults votingResults) {
        for(Object voteResult : votingResults.getVotingResults().values()) {
            push((VotingResult) voteResult);
        }
    }

    public void push(VotingResult votingResult) {
        restTemplate.postForLocation(ConnectionInfo.CLIENT_SOCKET_API_PUSH, votingResult);
    }

    public void currentDataVolume(long currentDataVolume) {
        CurrentDataVolumeCommand command = new CurrentDataVolumeCommand();
        command.currentDataVolume = currentDataVolume;
        restTemplate.postForLocation(ConnectionInfo.CLIENT_SOCKET_API_CURRENT_DATA_VOLUME, command);
    }
}
