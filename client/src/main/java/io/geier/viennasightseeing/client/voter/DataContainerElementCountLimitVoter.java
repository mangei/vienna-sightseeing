package io.geier.viennasightseeing.client.voter;

import io.geier.vbs.*;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataContainerElementCountLimitVoter extends DefaultVoter<SightMetaInfo> {

    private static Logger LOG = LoggerFactory.getLogger(DataContainerElementCountLimitVoter.class);

    @Autowired
    private ClientService contextService;

    /**
     * If the current element count is the maximum limit, it will vote for NO, otherwise NONE
     */
    @Override
    public Vote shouldStoreOnGet(SightMetaInfo metaInfo, DataContainer dataContainer) {
        long limit = contextService.getContext().getElementCountLimit();
        if (dataContainer.getStore().size() >= limit) {
            return createVote(VoteType.NO, 100, "exceeds element count limit");
        }
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldPreload(SightMetaInfo metaInfo, DataContainer<SightMetaInfo, ?> dataContainer) {
        return shouldStoreOnGet(metaInfo, dataContainer);
    }

    @Override
    public Vote shouldUpdate(SightMetaInfo metaInfo, DataContainer<SightMetaInfo, ?> dataContainer) {
        return shouldStoreOnGet(metaInfo, dataContainer);
    }

    /**
     * Elements can always be removed
     */
    @Override
    public Vote shouldRemove(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    protected Voter<SightMetaInfo> getVoter() {
        return this;
    }

    private Vote createVote(VoteType voteType) {
        return createVote(voteType, Vote.DEFAULT_MULTIPLIER, "");
    }

    private Vote createVote(VoteType voteType, int multiplier, String reason) {
        reason = "["+getClass().getSimpleName()+"] " + reason;
        LOG.info("vote: "+voteType+"("+multiplier+"): " + reason);
        return VoteFactory.create(this, voteType, multiplier, reason);
    }
}
