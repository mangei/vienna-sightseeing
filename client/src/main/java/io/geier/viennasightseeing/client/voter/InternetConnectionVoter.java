package io.geier.viennasightseeing.client.voter;

import io.geier.vbs.*;
import io.geier.viennasightseeing.client.entity.Context;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.service.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InternetConnectionVoter extends DefaultVoter<SightMetaInfo> {

    private static Logger LOG = LoggerFactory.getLogger(InternetConnectionVoter.class);

    @Autowired
    private ClientService contextService;

    @Override
    public Vote shouldStoreOnGet(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldPreload(SightMetaInfo metaInfo, DataContainer dataContainer) {
        Context.InternetConnectionType internetConnectionType = contextService.getContext().getInternetConnectionType();
        switch (internetConnectionType) {
            case WiFi:
                return createVote(VoteType.YES, 30, "is WiFi");
            case Mobile:
                return createVote(VoteType.NO, 100, "is Mobile");
            case Roaming:
                return createVote(VoteType.NO, 100, "is Roaming");
            case None:
                return createVote(VoteType.NO, 100, "is None");
        }
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldUpdate(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldRemove(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    protected Voter<SightMetaInfo> getVoter() {
        return this;
    }

    private Vote createVote(VoteType voteType) {
        return createVote(voteType, Vote.DEFAULT_MULTIPLIER, "");
    }

    private Vote createVote(VoteType voteType, int multiplier, String reason) {
        reason = "["+getClass().getSimpleName()+"] " + reason;
        LOG.info("vote: "+voteType+"("+multiplier+"): " + reason);
        return VoteFactory.create(this, voteType, multiplier, reason);
    }

}
