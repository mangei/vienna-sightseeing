'use strict';

angular.module('sightApp')
  .controller('ClientController', function ($scope, $route, clientService, growl) {
    var data = {};
    $scope.data = data;
    data.context = {};
    data.logs = [];
    data.logSize = 0;

    var socket = io.connect("http://localhost:8090/");
    socket.on('log', function (log) {
        log.timestamp = new Date();
        data.logs.push(log);
        $scope.$apply();
    });
    socket.on('currentDataVolume', function (res) {
        data.context.currentDataVolume = res.data.currentDataVolume;
        $scope.$apply();
    });

    $scope.reload = function(addSuccessMessage) {
        clientService.getContext().success(function(context) {
            data.context = context;
            if(addSuccessMessage === undefined || !!addSuccessMessage)
                growl.addSuccessMessage('Context reset successful');
        });
    };

    $scope.save = function() {
        clientService.saveContext(data.context).then(function() {
            growl.addSuccessMessage('Context successfully saved');
        });
    };

    $scope.preload = function() {
        clientService.preload().then(function() {
            growl.addSuccessMessage('Data container preloaded');
        });
    };

     $scope.cleanup = function() {
         clientService.cleanup().then(function() {
             growl.addSuccessMessage('Data container cleaned up');
         });
     };

    $scope.showDetails = function(showDetails) {
        for(var i=0; $scope.data.logs.length; i++) {
            $scope.data.logs[i].showDetails = showDetails;
        }
    };

    $scope.resetDataVolume = function(showDetails) {
        clientService.resetDataVolume().then(function() {
            growl.addSuccessMessage('Used mobile data volume reset successful');
        });
    };

    $scope.reload(false);
  })
;
