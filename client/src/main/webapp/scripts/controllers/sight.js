'use strict';

angular.module('sightApp')
    .controller('SightController', function ($scope, $location, $routeParams, sightService, growl) {

        $scope.data = {};
        $scope.view = "";

        var defaults = {
            goToZoom: 17
        };

        var list = function() {
            $scope.data.sights = [];
            $scope.data.mapMarkers = [];
            $scope.data.mapCenter = {
                lat: 48.20846196689827,
                lng: 16.373149077512693,
                zoom: 14
            };

            var loadSights = function() {
                sightService.list().success(function(sights) {
                    var sightsSize = sights.length;
                    for(var i=0; i<sightsSize; i++) {
                        var sight = sights[i];
                        var marker = {
                            message: sight.title,
                            lat: sight.latitude,
                            lng: sight.longitude
                        };
                        $scope.data.mapMarkers.push(marker);
                    }
                    $scope.data.sights = sights;
                });
            };
            loadSights();
        };

        var get = function(id) {
            $scope.data.sight = {};
            $scope.data.mapMarkers = [];
            $scope.data.mapCenter = {};
            sightService.get(id).success(function(sight) {
                $scope.data.sight = sight;
                $scope.data.mapCenter = {
                    lat: sight.latitude,
                    lng: sight.longitude,
                    zoom: 17
                };
            });
        };

        if($routeParams.id) {
            $scope.view = "show";
            var id = $routeParams.id;
            get(id);
        } else {
            $scope.view = "list";
            list();
        }

        $scope.goToSight = function(sight) {
            $scope.data.mapCenter.lat = sight.latitude;
            $scope.data.mapCenter.lng = sight.longitude;
            $scope.data.mapCenter.zoom = defaults.goToZoom;
        };

        $scope.toggleStar = function(sight) {
            if($scope.view === "show") {
                if(sight.metaInfo.stared) {
                    sightService.unstar(sight.id).success(function(data) {
                        sight.metaInfo.stared = !sight.metaInfo.stared;
                    });
                } else {
                    sightService.star(sight.id).success(function(data) {
                        sight.metaInfo.stared = !sight.metaInfo.stared;
                    });
                }
            } else if ($scope.view === "list") {
                if(sight.stared) {
                    sightService.unstar(sight.id).success(function(data) {
                        sight.stared = !sight.stared;
                    });
                } else {
                    sightService.star(sight.id).success(function(data) {
                        sight.stared = !sight.stared;
                    });
                }
            }
        };

        $scope.saveNextVisitDate = function(sight) {
            sightService.saveNextVisitDate(sight.id, sight.metaInfo.nextVisitDate).success(function(data) {
                growl.addSuccessMessage('Next visit date successfully saved');
            });
        };
    })
;
