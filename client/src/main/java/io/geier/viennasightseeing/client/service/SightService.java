package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.Sight;

public interface SightService {
    Sight get(Long id);
    void preload();
    void cleanup();
}
