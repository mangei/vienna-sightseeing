package io.geier.viennasightseeing.client.entity;

import java.util.Date;

public class Context {

    public enum InternetConnectionType {
        None, Roaming, Mobile, WiFi
    }

    private Date currentDate = new Date();
    private int nearFutureInDays = 14;
    private long dataSizeLimit = 10000;
    private long elementCountLimit = 100;
    private InternetConnectionType internetConnectionType = InternetConnectionType.WiFi;
    private long dataVolumeLimit = -1;
    private long currentDataVolume = 0;

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public int getNearFutureInDays() {
        return nearFutureInDays;
    }

    public void setNearFutureInDays(int nearFutureInDays) {
        this.nearFutureInDays = nearFutureInDays;
    }

    public long getDataSizeLimit() {
        return dataSizeLimit;
    }

    public void setDataSizeLimit(long dataSizeLimit) {
        this.dataSizeLimit = dataSizeLimit;
    }

    public long getElementCountLimit() {
        return elementCountLimit;
    }

    public void setElementCountLimit(long elementCountLimit) {
        this.elementCountLimit = elementCountLimit;
    }

    public InternetConnectionType getInternetConnectionType() {
        return internetConnectionType;
    }

    public void setInternetConnectionType(InternetConnectionType internetConnectionType) {
        this.internetConnectionType = internetConnectionType;
    }

    public long getDataVolumeLimit() {
        return dataVolumeLimit;
    }

    public void setDataVolumeLimit(long dataVolumeLimit) {
        this.dataVolumeLimit = dataVolumeLimit;
    }

    public long getCurrentDataVolume() {
        return currentDataVolume;
    }

    public void setCurrentDataVolume(long currentDataVolume) {
        this.currentDataVolume = currentDataVolume;
    }
}
