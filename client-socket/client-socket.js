var express = require('express');
var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);
var jsonBody = require("body/json")

server.listen(8090);

app.post('/push', function(req, res) {
    jsonBody(req, res, function(err, body) {
        console.log(body);
        io.sockets.emit('log', {data: body});
    });
    res.end();
});

app.post('/currentDataVolume', function(req, res) {
    jsonBody(req, res, function(err, body) {
        console.log(body);
        io.sockets.emit('currentDataVolume', {data: body});
    });
    res.end();
});