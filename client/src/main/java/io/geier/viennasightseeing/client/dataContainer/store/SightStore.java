package io.geier.viennasightseeing.client.dataContainer.store;

import io.geier.vbs.Store;
import io.geier.viennasightseeing.client.entity.Sight;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.service.SightMetaInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a store for all sights. It also saved the data size of all elements.
 */
@Component
public class SightStore implements Store<SightMetaInfo, Sight> {

    private static Logger LOG = LoggerFactory.getLogger(SightStore.class);

    @Autowired
    private SightMetaInfoService sightMetaInfoService;

    private Map<Long, Sight> store = new HashMap<Long, Sight>();
    private long dataSize = 0;

    @Override
    public Sight get(SightMetaInfo metaInfo) {
        return store.get(metaInfo.getId());
    }

    @Override
    public Map<SightMetaInfo, Sight> getAll(List<SightMetaInfo> sightMetaInfos) {
        Map<SightMetaInfo, Sight> sights = new HashMap<SightMetaInfo ,Sight>();
        for(SightMetaInfo metaInfo : sightMetaInfos) {
            sights.put(metaInfo, get(metaInfo));
        }
        return sights;
    }

    @Override
    public void store(SightMetaInfo metaInfo, Sight sight) {
        if(metaInfo != null && sight != null) {
            store.put(metaInfo.getId(), sight);
            metaInfo.setStored(true);
            sightMetaInfoService.save(metaInfo);
            dataSize += metaInfo.getSize();
            LOG.info("Total data size used: " + dataSize);
        } else {
            LOG.info("nothing stored, because metaInfo or sight was null");
        }
    }

    @Override
    public void storeAll(Map<SightMetaInfo, Sight> entries) {
        for (Map.Entry<SightMetaInfo, Sight> entry : entries.entrySet()) {
            store(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public long size() {
        return store.size();
    }

    @Override
    public List<SightMetaInfo> getAllMetaInfos() {
        return sightMetaInfoService.findAllByStored(true);
    }

    @Override
    public void remove(SightMetaInfo metaInfo) {
        store.remove(metaInfo.getId());
        metaInfo.setStored(false);
        sightMetaInfoService.save(metaInfo);
        dataSize -= metaInfo.getSize();
        LOG.info("Total data size used: " + dataSize);
    }

    @Override
    public void removeAll(List<SightMetaInfo> sightMetaInfos) {
        for (SightMetaInfo metaInfo : sightMetaInfos) {
            remove(metaInfo);
        }
    }

    @Override
    public boolean containsMetaInfo(SightMetaInfo metaInfo) {
        return store.containsKey(metaInfo.getId());
    }

    @Override
    public Map<SightMetaInfo, Boolean> containsMetaInfos(List<SightMetaInfo> sightMetaInfos) {
        Map<SightMetaInfo, Boolean> resultMap = new HashMap<SightMetaInfo, Boolean>();
        for(SightMetaInfo metaInfo : sightMetaInfos) {
            resultMap.put(metaInfo, store.containsKey(metaInfo.getId()));
        }
        return resultMap;
    }

    public long getDataSize() {
        return dataSize;
    }
}