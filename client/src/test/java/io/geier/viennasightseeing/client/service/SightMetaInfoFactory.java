package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.SightMetaInfo;

public class SightMetaInfoFactory {

    public static SightMetaInfo createStaredSightMetaInfo() {
        return createSightMetaInfo(true);
    }

    public static SightMetaInfo createUnstaredSightMetaInfo() {
        return createSightMetaInfo(true);
    }

    private static SightMetaInfo createSightMetaInfo(boolean starred) {
        SightMetaInfo metaInfo = new SightMetaInfo();
        metaInfo.setStared(starred);
        return metaInfo;
    }
}
