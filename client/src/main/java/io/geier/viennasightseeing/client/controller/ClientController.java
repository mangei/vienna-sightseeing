package io.geier.viennasightseeing.client.controller;

import io.geier.viennasightseeing.client.entity.Context;
import io.geier.viennasightseeing.client.service.ClientService;
import io.geier.viennasightseeing.client.service.SightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/client/")
public class ClientController {

    @Autowired
    ClientService clientService;
    @Autowired
    SightService sightService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Context get() {
        return clientService.getContext();
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public Context save(@RequestBody Context context) {
        return clientService.save(context);
    }

    @RequestMapping(value="preload", method = RequestMethod.POST)
    @ResponseBody
    public void preload() {
        sightService.preload();
    }

    @RequestMapping(value="cleanup", method = RequestMethod.POST)
    @ResponseBody
    public void cleanup() {
        sightService.cleanup();
    }

    @RequestMapping(value="resetDataVolume", method = RequestMethod.POST)
    @ResponseBody
    public void resetDataVolume() {
        clientService.resetDataConsumption();
    }

}
