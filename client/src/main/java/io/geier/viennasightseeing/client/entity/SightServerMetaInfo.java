package io.geier.viennasightseeing.client.entity;

/**
 * This is a light meta info object for a Sight
 */
public class SightServerMetaInfo {

    private Long id;
    private String title;
    private Long size;
    private float longitude;
    private float latitude;

    public SightServerMetaInfo() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
