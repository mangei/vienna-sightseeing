# Vienna Sightseeing Application

by Manuel Geier (http://geier.io)

This is a proof-of-concept application to evaluate the VBS framework:  
http://bitbucket.org/mangei/voting-based-storage-framework

# Getting started

## Installs

Get Git  
http://git-scm.com

Get Gradle  
http://www.gradle.org/

Get NodeJS  
http://http://nodejs.org/

Get MongoDB  
http://www.mongodb.org/

## Initialization

Install Bower (http://bower.io/)  
`npm install -g bower`

Get project files from:  
`https://bitbucket.org/mangei/bac-example`

Initialize the wrapper for Gradle:  
`gradle wrapper`

To make it ready for Intellij IDEA run:  
`gradlew client:idea`

Install packages  
in `./server`:  
`npm install`  
in `./client-socket`:   
`npm install`

in `./client/src/main/webapp/WEB-INF`:  
`bower install --config.interactive=false`

If there is a git error and you cannot bower all modules, try this:  
`git config --global url."https://".insteadOf git://`

# Run the application

You have to start the following components to run the application.

## Client

In `./client`:  
`gradlew bootRun`

## Client-Socket

In `./client-socket`:  
`node client-socket.js`

## Server

In `./server`:  
`node server.js`

## MongoDB

Start your MongoDB instance (with default port: 27017).

# See the application

Server: `http://localhost:3000`  
Client: `http://localhost:8080` (Client frontend)  
Client: `http://localhost:8080/#/control` (Client control)
