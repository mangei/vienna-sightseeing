package io.geier.viennasightseeing;

/**
 * Holds an object.
 */
public class ObjectHolder {
    public Object object;
}
