package io.geier.viennasightseeing.client.repository;

import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SightMetaInfoRepository extends MongoRepository<SightMetaInfo, Long> {
    SightMetaInfo findById(Long id);
    List<SightMetaInfo> findAllByStored(boolean stored);
}
