package io.geier.viennasightseeing.client.voter;

import io.geier.vbs.*;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.service.ClientService;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * If the sight is used in the near future, the element should be saved, and not removed
 */
@Component
public class NearFutureDateVoter extends DefaultVoter<SightMetaInfo> {

    private static Logger LOG = LoggerFactory.getLogger(NearFutureDateVoter.class);

    @Autowired
    private ClientService contextService;

    @Override
    public Vote shouldStoreOnGet(SightMetaInfo metaInfo, DataContainer dataContainer) {
        if (isInNearFuture(metaInfo)) {
            return createVote(VoteType.YES, 10, "is in near future");
        } else {
            return createVote(VoteType.NO, 10, "is not in near future");
        }
    }

    @Override
    public Vote shouldPreload(SightMetaInfo metaInfo, DataContainer<SightMetaInfo, ?> dataContainer) {
        return shouldStoreOnGet(metaInfo, dataContainer);
    }

    @Override
    public Vote shouldUpdate(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldRemove(SightMetaInfo metaInfo, DataContainer dataContainer) {
        if (isInNearFuture(metaInfo)) {
            return createVote(VoteType.NO, 30, "is in near future");
        }
        return createVote(VoteType.NONE);
    }

    private boolean isInNearFuture(SightMetaInfo metaInfo) {
        Date nextVisitDate = metaInfo.getNextVisitDate();
        if(nextVisitDate != null) {
            Date currentDate = contextService.getContext().getCurrentDate();
            int nearFutureInDays = contextService.getContext().getNearFutureInDays();
            return isWithinNextNDays(currentDate, nextVisitDate, nearFutureInDays);
        } else {
            return false;
        }
    }

    @Override
    protected Voter<SightMetaInfo> getVoter() {
        return this;
    }

    private boolean isWithinNextNDays(Date currentDate, Date date, int nextNDays) {
        Date futureDate = DateUtils.truncate(DateUtils.addDays(currentDate, nextNDays), Calendar.DAY_OF_MONTH);
        return date.after(currentDate) && date.before(futureDate);
    }

    private Vote createVote(VoteType voteType) {
        return createVote(voteType, Vote.DEFAULT_MULTIPLIER, "");
    }

    private Vote createVote(VoteType voteType, int multiplier, String reason) {
        reason = "["+getClass().getSimpleName()+"] " + reason;
        LOG.info("vote: "+voteType+"("+multiplier+"): " + reason);
        return VoteFactory.create(this, voteType, multiplier, reason);
    }

}
