package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.Context;

public interface ClientService {
    Context getContext();

    Context save(Context context);

    void increaseDataConsumption(long consumption);

    void resetDataConsumption();
}
