package io.geier.viennasightseeing.client.service;

import io.geier.vbs.*;
import io.geier.viennasightseeing.client.dataContainer.store.SightStore;
import io.geier.viennasightseeing.client.entity.Context;
import io.geier.viennasightseeing.client.entity.Sight;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.repository.ClientRepository;
import io.geier.viennasightseeing.client.repository.SightRepository;
import io.geier.viennasightseeing.client.voter.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SightServiceImpl implements SightService {

    private SightRepository sightRepository;
    private SightMetaInfoService sightMetaInfoService;
    private ClientService clientService;
    private ClientRepository clientRepository;

    private DataContainer<SightMetaInfo, Sight> dataContainer;

    @Autowired
    public SightServiceImpl(ApplicationContext applicationContext, SightRepository sightRepository, SightMetaInfoService sightMetaInfoService, ClientService clientService, ClientRepository clientRepository) {
        this.sightRepository = sightRepository;
        this.sightMetaInfoService = sightMetaInfoService;
        this.clientService = clientService;
        this.clientRepository = clientRepository;

        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();

        SightStore sightStore = beanFactory.createBean(SightStore.class);
        List<Voter<SightMetaInfo>> voters = Arrays.asList(
                (Voter<SightMetaInfo>) beanFactory.createBean(StarVoter.class),
                (Voter<SightMetaInfo>) beanFactory.createBean(DataContainerDataSizeLimitVoter.class),
                (Voter<SightMetaInfo>) beanFactory.createBean(DataContainerElementCountLimitVoter.class),
                (Voter<SightMetaInfo>) beanFactory.createBean(NearFutureDateVoter.class),
                (Voter<SightMetaInfo>) beanFactory.createBean(InternetConnectionVoter.class),
                (Voter<SightMetaInfo>) beanFactory.createBean(DataVolumeLimitVoter.class)
        );
        Fetcher<SightMetaInfo, Sight> fetcher = new Fetcher<SightMetaInfo, Sight>() {
            @Override
            public Sight fetch(SightMetaInfo metaInfo) {
                return SightServiceImpl.this.fetch(metaInfo);
            }

            @Override
            public Map<SightMetaInfo, Sight> fetchAll(List<SightMetaInfo> metaInfos) {
                Map<SightMetaInfo, Sight> results = new HashMap<SightMetaInfo, Sight>();
                for(SightMetaInfo metaInfo : metaInfos) {
                    results.put(metaInfo, SightServiceImpl.this.fetch(metaInfo));
                }
                return results;
            }
        };
        dataContainer = new DataContainer<SightMetaInfo, Sight>(sightStore, fetcher, voters);
    }

    private Sight fetch(SightMetaInfo metaInfo) {
        if(clientService.getContext().getInternetConnectionType() == Context.InternetConnectionType.None) {
            throw new RuntimeException("no internet connection available");
        }
        Sight sight = sightRepository.findOne(metaInfo.getId());
        sight.setMetaInfo(sightMetaInfoService.getOrCreate(metaInfo));
        clientService.increaseDataConsumption(metaInfo.getSize());
        return sight;
    }

    public Sight get(Long id) {
        SightMetaInfo metaInfo = sightMetaInfoService.get(id);
        metaInfo = sightMetaInfoService.increaseAccessCount(metaInfo);
        Result<SightMetaInfo, Sight> result = dataContainer.get(metaInfo);
        clientRepository.push(result.getVotingResult());
        Sight sight = result.getDataObject();
        sight.setMetaInfo(metaInfo);
        return sight;
    }

    @Override
    public void preload() {
        List<SightMetaInfo> metaInfos = sightMetaInfoService.findAllByStored(false);
        VotingResults votingResults = dataContainer.preloadAll(metaInfos);
        clientRepository.push(votingResults);
    }

    @Override
    public void cleanup() {
        VotingResults votingResults = dataContainer.clear();
        clientRepository.push(votingResults);
    }

}
