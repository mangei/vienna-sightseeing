package io.geier.viennasightseeing.client.config;

public class ConnectionInfo {
    public static String SERVER_URL = "http://localhost:3000";
    public static String SERVER_API_SIGHTS = SERVER_URL + "/sights";
    public static String SERVER_API_SIGHTS_GET = SERVER_URL + "/sights/{id}";

    public static String CLIENT_SOCKET_URL = "http://localhost:8090";
    public static String CLIENT_SOCKET_API_PUSH = CLIENT_SOCKET_URL + "/push";
    public static String CLIENT_SOCKET_API_CURRENT_DATA_VOLUME = CLIENT_SOCKET_URL + "/currentDataVolume";
}
