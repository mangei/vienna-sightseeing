package io.geier.viennasightseeing.client.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.Date;

/**
 * This object saves all infos of the object
 */
public class SightMetaInfo extends SightServerMetaInfo {

    @Id
    @JsonIgnore
    private String localId;
    private boolean stared;
    private Long accessCount;
    private Date nextVisitDate;
    private boolean stored;

    public SightMetaInfo() {
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public void setId(String id) {
        this.localId = id;
    }

    public boolean isStared() {
        return stared;
    }

    public void setStared(boolean stared) {
        this.stared = stared;
    }

    public Long getAccessCount() {
        return accessCount;
    }

    public void setAccessCount(Long accessCount) {
        this.accessCount = accessCount;
    }

    public Date getNextVisitDate() {
        return nextVisitDate;
    }

    public void setNextVisitDate(Date nextVisitDate) {
        this.nextVisitDate = nextVisitDate;
    }

    public boolean isStored() {
        return stored;
    }

    public void setStored(boolean stored) {
        this.stored = stored;
    }
}
