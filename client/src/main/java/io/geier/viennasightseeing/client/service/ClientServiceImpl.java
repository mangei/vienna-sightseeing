package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.Context;
import io.geier.viennasightseeing.client.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    private Context context;

    public ClientServiceImpl() {
        this.context = new Context();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public Context save(Context context) {
        this.context = context;
        return context;
    }

    @Override
    public void increaseDataConsumption(long consumption) {
        if(context.getInternetConnectionType() == Context.InternetConnectionType.Mobile ||
                context.getInternetConnectionType() == Context.InternetConnectionType.Roaming) {
            context.setCurrentDataVolume(context.getCurrentDataVolume() + consumption);
            clientRepository.currentDataVolume(context.getCurrentDataVolume());
        }
    }

    @Override
    public void resetDataConsumption() {
        context.setCurrentDataVolume(0);
        clientRepository.currentDataVolume(context.getCurrentDataVolume());
    }

}
