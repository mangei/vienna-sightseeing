package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.entity.SightServerMetaInfo;

import java.util.List;

public interface SightMetaInfoService {
    SightMetaInfo get(Long id);

    SightMetaInfo getOrCreate(SightServerMetaInfo serverMetaInfo);

    SightMetaInfo save(SightMetaInfo metaInfo);

    List<SightMetaInfo> getAll();

    SightMetaInfo star(SightMetaInfo metaInfo);

    SightMetaInfo unstar(SightMetaInfo metaInfo);

    SightMetaInfo increaseAccessCount(SightMetaInfo metaInfo);

    List<SightMetaInfo> findAllByStored(boolean stored);
}
