package io.geier.viennasightseeing.client.entity;


import java.util.List;

/**
 * This is a big data object.
 */
public class Sight {

    private Long id;
    private String title;
    private Long size;
    private float longitude;
    private float latitude;

    private String content;
    private List<String> images;

    private SightMetaInfo metaInfo;

    public Sight() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public SightMetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(SightMetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
