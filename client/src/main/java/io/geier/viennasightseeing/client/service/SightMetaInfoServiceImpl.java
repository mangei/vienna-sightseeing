package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.entity.SightServerMetaInfo;
import io.geier.viennasightseeing.client.repository.SightMetaInfoRepository;
import io.geier.viennasightseeing.client.repository.SightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;

@Service
public class SightMetaInfoServiceImpl implements SightMetaInfoService {

    SightMetaInfoRepository sightMetaInfoRepository;
    SightRepository sightRepository;

    @Autowired
    public SightMetaInfoServiceImpl(SightMetaInfoRepository sightMetaInfoRepository, SightRepository sightRepository) {
        this.sightMetaInfoRepository = sightMetaInfoRepository;
        this.sightRepository = sightRepository;

        sightMetaInfoRepository.deleteAll();
    }

    @Override
    public SightMetaInfo get(Long id) {
        return sightMetaInfoRepository.findById(id);
    }

    @Override
    public SightMetaInfo getOrCreate(SightServerMetaInfo serverMetaInfo) {
        SightMetaInfo metaInfo = get(serverMetaInfo.getId());
        if (metaInfo == null) {
            metaInfo = createNewSightLocalMetaInfo(serverMetaInfo);
            metaInfo = save(metaInfo);
        }
        return metaInfo;
    }

    private SightMetaInfo createNewSightLocalMetaInfo(SightServerMetaInfo serverMetaInfo) {
        SightMetaInfo metaInfo = new SightMetaInfo();
        metaInfo.setId(serverMetaInfo.getId());
        metaInfo.setTitle(serverMetaInfo.getTitle());
        metaInfo.setSize(serverMetaInfo.getSize());
        metaInfo.setLongitude(serverMetaInfo.getLongitude());
        metaInfo.setLatitude(serverMetaInfo.getLatitude());
        metaInfo.setAccessCount(0L);
        metaInfo.setStared(false);
        return metaInfo;
    }

    public List<SightMetaInfo> getAll() {
        try {
            List<SightServerMetaInfo> metaInfos = sightRepository.findAll();
            List<SightMetaInfo> localMetaInfos = new ArrayList<SightMetaInfo>();
            // create LocalMetaInfo objects if they do not exists
            for (SightServerMetaInfo metaInfo : metaInfos) {
                localMetaInfos.add(getOrCreate(metaInfo));
            }
            return localMetaInfos;
        } catch (ResourceAccessException e) {
            return sightMetaInfoRepository.findAll();
        }
    }

    @Override
    public SightMetaInfo star(SightMetaInfo metaInfo) {
        metaInfo.setStared(true);
        return save(metaInfo);
    }

    @Override
    public SightMetaInfo unstar(SightMetaInfo metaInfo) {
        metaInfo.setStared(false);
        return save(metaInfo);
    }

    @Override
    public SightMetaInfo increaseAccessCount(SightMetaInfo metaInfo) {
        metaInfo.setAccessCount(metaInfo.getAccessCount() + 1);
        return save(metaInfo);
    }

    @Override
    public List<SightMetaInfo> findAllByStored(boolean stored) {
        return sightMetaInfoRepository.findAllByStored(stored);
    }

    public SightMetaInfo save(SightMetaInfo metaInfo) {
        return sightMetaInfoRepository.save(metaInfo);
    }
}
