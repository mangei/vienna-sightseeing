var express = require('express'),
	sight = require('./routes/sights'),
	jade = require('jade');
 
var app = express();
 
app.configure(function () {
	app.use(express.logger('dev')); /* 'default', 'short', 'tiny', 'dev' */
	app.use(express.bodyParser());
});
 
app.get('/', function(req, res) {
	res.sendfile('index.html');
});
app.get('/sights', sight.findAll);
app.get('/sights/:id', sight.findById);
 
app.listen(3000);
console.log('Listening on port 3000...');