package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;


public abstract class SightMetaInfoServiceTest {

    protected abstract SightMetaInfoService getService();

    @Test
    public void starIfNotStarred() {
        SightMetaInfo metaInfo = SightMetaInfoFactory.createUnstaredSightMetaInfo();

        metaInfo = getService().star(metaInfo);

        assertNotNull(metaInfo);
        assertTrue(metaInfo.isStared());
    }

    @Test
    public void starIfStarred() {
        SightMetaInfo metaInfo = SightMetaInfoFactory.createStaredSightMetaInfo();

        metaInfo = getService().star(metaInfo);

        assertNotNull(metaInfo);
        assertTrue(metaInfo.isStared());
    }

    @Test
    public void unstarIfNotStarred() {
        SightMetaInfo metaInfo = SightMetaInfoFactory.createUnstaredSightMetaInfo();

        metaInfo = getService().unstar(metaInfo);

        assertNotNull(metaInfo);
        assertFalse(metaInfo.isStared());
    }

    @Test
    public void unstarIfStarred() {
        SightMetaInfo metaInfo = SightMetaInfoFactory.createStaredSightMetaInfo();

        metaInfo = getService().unstar(metaInfo);

        assertNotNull(metaInfo);
        assertFalse(metaInfo.isStared());
    }

    @Test
    @Ignore(value = "check mocking why save returns null")
    public void getObjectWhichWasSaved() {
        SightMetaInfo metaInfo = SightMetaInfoFactory.createStaredSightMetaInfo();
        metaInfo = getService().save(metaInfo);
        Long id = metaInfo.getId();

        metaInfo = getService().get(id);

        assertNotNull(metaInfo);
    }

    @Test
    public void getNullWithNoSavedObject() {
        Long id = new Long(1);

        SightMetaInfo metaInfo = getService().get(id);

        assertNull(metaInfo);
    }

}
