'use strict';

angular.module('sightApp')
  .factory('sightService', function ($http) {
    var url = "http://localhost:8080/sights/";
    return {
        list: function() {
            return $http.get(url).error(function(data, status, headers, config) {
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
            });
        },
        get: function(id) {
            return $http.get(url+id);
        },
        star: function(id) {
            return $http.put(url+id+"/star");
        },
        unstar: function(id) {
            return $http.put(url+id+"/unstar");
        },
        saveNextVisitDate: function(id, date) {
            return $http.put(url+id+"/saveNextVisitDate", {'nextVisitDate': date});
        }
    };
  })
;
