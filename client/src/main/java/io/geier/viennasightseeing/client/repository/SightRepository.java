package io.geier.viennasightseeing.client.repository;

import io.geier.viennasightseeing.client.entity.Sight;
import io.geier.viennasightseeing.client.entity.SightServerMetaInfo;

import java.util.List;

public interface SightRepository {
    Sight findOne(Long id);

    List<SightServerMetaInfo> findAll();
}
