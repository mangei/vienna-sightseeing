package io.geier.viennasightseeing.client.voter;

import io.geier.vbs.*;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * This is a user defined voter. Based on the user decision to star a sight, the voter votes for it.
 */
@Component
public class StarVoter extends DefaultVoter<SightMetaInfo> {

    private static Logger LOG = LoggerFactory.getLogger(StarVoter.class);

    @Override
    public Vote shouldStoreOnGet(SightMetaInfo metaInfo, DataContainer dataContainer) {
        if (metaInfo.isStared()) {
            return createVote(VoteType.YES, 20, "is stared");
        } else {
            return createVote(VoteType.NO, 20, "is not stared");
        }
    }

    @Override
    public Vote shouldPreload(SightMetaInfo metaInfo, DataContainer<SightMetaInfo, ?> dataContainer) {
        return shouldStoreOnGet(metaInfo, dataContainer);
    }

    @Override
    public Vote shouldUpdate(SightMetaInfo metaInfo, DataContainer dataContainer) {
        return createVote(VoteType.NONE);
    }

    @Override
    public Vote shouldRemove(SightMetaInfo metaInfo, DataContainer dataContainer) {
        if (!metaInfo.isStared()) {
            return createVote(VoteType.YES, 20, "is not stared");
        }
        return createVote(VoteType.NONE);
    }

    @Override
    protected Voter<SightMetaInfo> getVoter() {
        return this;
    }

    private Vote createVote(VoteType voteType) {
        return createVote(voteType, Vote.DEFAULT_MULTIPLIER, "");
    }

    private Vote createVote(VoteType voteType, int multiplier, String reason) {
        reason = "["+getClass().getSimpleName()+"] " + reason;
        LOG.info("vote: "+voteType+"("+multiplier+"): " + reason);
        return VoteFactory.create(this, voteType, multiplier, reason);
    }
}
