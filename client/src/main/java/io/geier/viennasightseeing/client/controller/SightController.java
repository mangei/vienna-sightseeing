package io.geier.viennasightseeing.client.controller;

import io.geier.viennasightseeing.client.command.UpdateSightNextVisitDateCommand;
import io.geier.viennasightseeing.client.entity.Sight;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.service.SightMetaInfoService;
import io.geier.viennasightseeing.client.service.SightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/sights/")
public class SightController {

    @Autowired
    SightService sightService;
    @Autowired
    SightMetaInfoService sightMetaInfoService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<SightMetaInfo> list() {
        return sightMetaInfoService.getAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    @ResponseBody
    public Sight get(@PathVariable("id") Long id) {
        return sightService.get(id);
    }

    @RequestMapping(value = "{id}/star", method = RequestMethod.PUT)
    @ResponseBody
    public void star(@PathVariable("id") Long id) {
        SightMetaInfo metaInfo = sightMetaInfoService.get(id);
        if (metaInfo != null) {
            sightMetaInfoService.star(metaInfo);
        }
    }

    @RequestMapping(value = "{id}/unstar", method = RequestMethod.PUT)
    @ResponseBody
    public void unstar(@PathVariable("id") Long id) {
        SightMetaInfo metaInfo = sightMetaInfoService.get(id);
        if (metaInfo != null) {
            sightMetaInfoService.unstar(metaInfo);
        }
    }

    @RequestMapping(value = "{id}/saveNextVisitDate", method = RequestMethod.PUT)
    @ResponseBody
    public void saveNextVisitDate(@PathVariable("id") Long id, @RequestBody UpdateSightNextVisitDateCommand command) {
        SightMetaInfo metaInfo = sightMetaInfoService.get(id);
        if (metaInfo != null) {
            metaInfo.setNextVisitDate(command.nextVisitDate);
            SightMetaInfo sightMetaInfo = sightMetaInfoService.save(metaInfo);
        }
    }


}
