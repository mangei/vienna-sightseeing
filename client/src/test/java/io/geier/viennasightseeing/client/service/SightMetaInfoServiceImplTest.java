package io.geier.viennasightseeing.client.service;

import io.geier.viennasightseeing.ObjectHolder;
import io.geier.viennasightseeing.client.entity.SightMetaInfo;
import io.geier.viennasightseeing.client.repository.SightMetaInfoRepository;
import org.junit.Before;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SightMetaInfoServiceImplTest extends SightMetaInfoServiceTest {

    @InjectMocks
    private SightMetaInfoServiceImpl service;

    @Mock
    private SightMetaInfoRepository sightMetaInfoRepository;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Override
    protected SightMetaInfoService getService() {
        return service;
    }

    @Override
    public void starIfNotStarred() {
        Mockito.when(sightMetaInfoRepository.save((SightMetaInfo) Mockito.any())).then(AdditionalAnswers.returnsFirstArg());
        super.starIfNotStarred();
    }

    @Override
    public void starIfStarred() {
        Mockito.when(sightMetaInfoRepository.save((SightMetaInfo) Mockito.any())).then(AdditionalAnswers.returnsFirstArg());
        super.starIfStarred();
    }

    @Override
    public void unstarIfNotStarred() {
        Mockito.when(sightMetaInfoRepository.save((SightMetaInfo) Mockito.any())).then(AdditionalAnswers.returnsFirstArg());
        super.unstarIfNotStarred();
    }

    @Override
    public void unstarIfStarred() {
        Mockito.when(sightMetaInfoRepository.save((SightMetaInfo) Mockito.any())).then(AdditionalAnswers.returnsFirstArg());
        super.unstarIfStarred();
    }

    @Override
    public void getObjectWhichWasSaved() {
        final ObjectHolder savedObject = new ObjectHolder();
        Mockito.when(sightMetaInfoRepository.save((SightMetaInfo) Mockito.any())).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                savedObject.object = invocation.getArguments()[0];
                return savedObject.object;
            }
        });
        Mockito.when(sightMetaInfoRepository.findOne(Mockito.anyLong())).thenReturn((SightMetaInfo) savedObject.object);
        super.getObjectWhichWasSaved();
    }

    @Override
    public void getNullWithNoSavedObject() {
        super.getNullWithNoSavedObject();
    }
}
