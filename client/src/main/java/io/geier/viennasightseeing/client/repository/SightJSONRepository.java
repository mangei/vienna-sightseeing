package io.geier.viennasightseeing.client.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.geier.viennasightseeing.client.config.ConnectionInfo;
import io.geier.viennasightseeing.client.entity.Sight;
import io.geier.viennasightseeing.client.entity.SightServerMetaInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Repository
public class SightJSONRepository implements SightRepository {

    private RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    public SightJSONRepository() {
        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
        converters.add(new MappingJackson2HttpMessageConverter());
        restTemplate = new RestTemplate(converters);
    }

    public List<SightServerMetaInfo> findAll() {
        List<LinkedHashMap> result = restTemplate.getForObject(ConnectionInfo.SERVER_API_SIGHTS, List.class);
        List<SightServerMetaInfo> sights = new ArrayList<SightServerMetaInfo>();
        for (LinkedHashMap map : result) {
            SightServerMetaInfo sight = mapper.convertValue(map, SightServerMetaInfo.class);
            sights.add(sight);
        }
        return sights;
    }

    public Sight findOne(Long id) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", id.toString());
        return restTemplate.getForObject(ConnectionInfo.SERVER_API_SIGHTS_GET, Sight.class, params);
    }
}
