'use strict';

angular.module('sightApp')
  .factory('clientService', function ($http) {
    var url = "http://localhost:8080/client/";
    return {
      getContext: function() {
        return $http.get(url);
      },
      saveContext: function(context) {
        return $http.put(url, context);
      },
      preload: function() {
        return $http.post(url+"preload");
      },
      cleanup: function() {
        return $http.post(url+"cleanup");
      },
      resetDataVolume: function() {
         return $http.post(url+"resetDataVolume");
      }
    };
  })
;
